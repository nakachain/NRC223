pragma solidity ^0.5.4;

import "../token/NRC223Receiver.sol";

contract NRC223ReceiverMock is NRC223Receiver {
    bool public tokenFallbackExec;

    function tokenFallback(address from, uint amount, bytes calldata data) external {
        tokenFallbackExec = true;
    }
}
