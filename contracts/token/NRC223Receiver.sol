pragma solidity ^0.5.4;

/// @title NRC223 receiver interface
contract NRC223Receiver { 
    /// @dev Standard NRC223 function that will handle incoming token transfers.
    /// @param from Token sender address.
    /// @param amount Amount of tokens.
    /// @param data Transaction metadata.
    function tokenFallback(address from, uint amount, bytes calldata data) external;
}
