pragma solidity ^0.5.4;

import "../token/NRC223PreMinted.sol";

contract NRC223Factory {
    event NRC223TokenCreated(
        address indexed tokenAddress,
        string name,
        string symbol,
        uint8 decimals,
        uint256 totalSupply,
        address owner
    );

    function createNRC223PreMinted(
        string calldata name,
        string calldata symbol,
        uint8 decimals,
        uint256 totalSupply,
        address owner)
        external
        returns (NRC223PreMinted preMintToken)
    {
        NRC223PreMinted token = new NRC223PreMinted(name, symbol, decimals, totalSupply, owner);
        emit NRC223TokenCreated(address(token), name, symbol, decimals, totalSupply, owner);
        return token;
    }
}
