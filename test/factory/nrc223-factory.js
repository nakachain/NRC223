const { assert } = require('chai')
const TimeMachine = require('sol-time-machine')
const sassert = require('sol-assert')

const getConstants = require('../constants')
const NRC223Factory = require('../data/nrc223-factory')

contract('NRC223Factory', (accounts) => {
  const { OWNER, ACCT1, ACCT2, INVALID_ADDR } = getConstants(accounts)
  const timeMachine = new TimeMachine(web3)

  let factory

  beforeEach(async () => {
    await timeMachine.snapshot

    factory = new web3.eth.Contract(NRC223Factory.abi)
    factory = await factory.deploy({
      data: NRC223Factory.bytecode,
      arguments: [],
    }).send({ from: OWNER, gas: 4712388 })
  })

  afterEach(async () => {
    await timeMachine.revert
  })

  describe('createNRC223PreMinted', async () => {
    it('should create a new token contract', async () => {
      const tx1 = await factory.methods.createNRC223PreMinted(
        'TestToken1', 'TT1', 8, 10000000, ACCT1)
        .send({ from: ACCT1, gas: 4712388 })
      const token1Addr = tx1.events['NRC223TokenCreated'].returnValues['tokenAddress']
      assert.isDefined(token1Addr)
      assert.notEqual(token1Addr, INVALID_ADDR)
      sassert.event(tx1, 'NRC223TokenCreated')

      const tx2 = await factory.methods.createNRC223PreMinted(
        'TestToken2', 'TT2', 8, 10000000, ACCT2)
        .send({ from: ACCT2, gas: 4712388 })
      const token2Addr = tx2.events['NRC223TokenCreated'].returnValues['tokenAddress']
      assert.isDefined(token2Addr)
      assert.notEqual(token2Addr, INVALID_ADDR)
      sassert.event(tx2, 'NRC223TokenCreated')

      assert.notEqual(token1Addr, token2Addr)
    })
  })
})
